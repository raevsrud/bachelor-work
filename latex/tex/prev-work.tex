\chapter{Related Work}

 The topic of applying machine learning techniques to source code has been developing very rapidly lately. Referring to \textit{Papers With Code}\footnote{\url{https://paperswithcode.com}} artificial intelligence in the field of computer code is used in 39 tasks, such as code generation, documentation generation, program repair, source code summarization, etc.

 In the previous year, a model with the production name \textit{Github Copilot} (which is actually a \textit{Codex} \cite{codex}) became very popular. It's a GPT model fine-tuned on the source code from GitHub with 12B parameters. It can generate code based on a human description of the task in the form of a docstring and vice versa. The model weights and dataset are not publicly available.

 This year, the first open-source model comparable in size to the GPT-3 was presented by \textit{EleutherAI} \cite{gpt-neox-20b}.\textit{GPT-NeoX} is an autoregressive language model, which has 20B parameters. It has been trained on different types of data, including source code. The application of meta-learning techniques has recently become more popular. With using methods such as \textit{Few-Shot Learning}, we can effectively use this and other models for source code tasks.

 The team from \textit{Carnegie Mellon University} has released open-source 2.7B model called \textit{PolyCoder} \cite{polycoder}, which was trained on data for 12 Programming Languages. \textit{PolyCoder} outperforms GPT-3 in \textit{C Lang}.

 The authors of the previous two papers believe that open access to the data and source code of their projects is vital for developing this domain. Unfortunately, the field of machine learning is very costly because of the expensive hardware needed to train the models. Teams with access to such resources tend to achieve state-of-the-art, and this trend is rising.

 Another transformer model called \textit{AlphaCode} \cite{alphacode} raised the interest in solving the problem of competitive programming. The largest of the models presented by \textit{DeepMind} has 41.1B parameters.

\section{Source Code vs. Nature Language}
\label{section:code_vs_nl}
 Programming source code is basically a sequence of instructions or sequence of tokens, which is very similar to human language but uses certain rules and patterns which the compiler or interpreter will understand. Therefore, Nature Language Processing (NLP) tasks are comparable to the source code processing tasks (Table \ref{table:sc-nl-hierarchy}). In both domains, the same thing can be expressed in different ways. 

 \begin{table}[h!]
     \centering
     \begin{tabular}{ |c|c| } 
         \hline
         character & character \\
          \hline
         token/lexeme & word \\
          \hline
         statement & sentence \\
          \hline
         code block & paragraph \\
          \hline
         source code file & document \\
         \hline
         corpus/repository & corpus \\
         \hline
     \end{tabular}
     \caption{Source Code vs. Nature Language Hierarchy.\label{table:sc-nl-hierarchy}}
 \end{table}

\section{The Naturalness Hypothesis}
\newtheorem*{naturlness}{The Naturalness Hypothesis}
\begin{naturlness}
	Software is a form of human communication; software corpora have similar statistical properties to natural language corpora; and these properties can be exploited to build better software engineering tools.
\end{naturlness}
This hypothesis was introduced in the article "A survey of machine learning for big code and naturalness" \cite{big_code}.

Code is considered good if it optimally performs its task and is written in a clear and transparent way. Usually, this style is very similar to the way a person would try to explain something, but only using a specific syntax. 

The meaning of program semantics and human semantics are different, so we need to make a clear distinction between them.

The variables and function names have no effect on the program performance and functionality, but they affect the readability of the code. The same thing can be elegantly written but work slowly, or incomprehensibly and terribly written but work much faster. There are still problems that require expert knowledge, and no model can handle this and find the optimal solution for such a task.

\section{Code Abstractions Diversity}

There are several model types of models that work at different abstraction levels:

\begin{description}
    \item[Token] Working with token sequences (n-gram, Recurrent Neural Networks, Transformers). 
    \item[Structural/Syntactic] The input data are ASTs (e.g. \textit{code2vec} \cite{code2vec}).
    \item[Semantic] Generalization of two previous levels. This model type working with graph data (Graph Neural Networks).
\end{description}

Different models that work at these levels will be discussed in the following section.
Then in Section \ref{sec:r-ast-roberta}, there will be proposed a model that works with AST at the token level. It combines two levels - Structural and Token levels.

\section{Code Generating Models}
\subsection{N-gram model}
N-gram\cite{n-gram} is a simple probabilistic language model predicting the next token by giving a sequence of ($n - 1$) tokens.
\[ 
	P(x_i \,|\, x_{i-(n-1)}, \,\dots, x_{i-1} ).
\]
Before training, it is recommended to use an appropriate preprocessing pipeline.
The next step is to count how many times each n-gram occurs. In the case of the bi-gram model (1-word context) and having these frequencies, it is easy to calculate which word occurs more often after our target word. For  \verb|(n > 2)|-grams the probability chain rule is used. 

N-gram language model is very context-sensitive and is highly dependent on \verb|n| parameter. The greater the value of \verb|n|, the better the model, but it significantly increases the memory load. Also, code can be very comprehensive and detailed, so context gets lost very quickly.

Since all unique tokens cannot have connections to all others in a vocabulary, n-grams are sparse. This leads to the following problem the probability of a word that is not in the corpus is $0$.
The solution to this problem is a technique called \textit{smoothing}.
The \textit{Laplacian Smoothing} is a simple approach in which the $1$ is added to every zero frequency.

In the code domain, such a model may produce syntactically incorrect results, which also depend on preprocessing step \cite{big_code}.

This model and its variations are widely used by source-code editors and plugins because it is a simple and lightweight model, but still, it is not effective as it only pays attention to the previous code tokens.

\subsection{RNN}
Recurrent Neural Networks \cite{rnn} are networks with loops that are well suited for processing sequences (Figure \ref{fig:rnn}).

\begin{figure}[H]
    \centering
    \adjustbox{width=\textwidth, center=\textwidth}{%
    \setlength{\fboxsep}{4pt}%
	\setlength{\fboxrule}{.2pt}%
    \fbox{\includegraphics[width=\textwidth]{RNN}}}
    \caption{RNN Block.\label{fig:rnn}}
\end{figure}

RNN training is similar to training an ordinary neural network, but with a slight modification to the error backpropagation algorithm \cite{neural-network}. Since the same parameters are used at all temporal steps in the network, the gradient at each output depends not only on the computation of the current step but also on the previous temporal steps. Therefore, we need to keep track of the gradients not only at the current step but also at all previous steps. This algorithm is called Backpropagation Through Time or \textit{BPTT} \cite{bptt}.

The \textit{Long Term Memory Network} (LSTM) \cite{lstm} is the most popular RNN architecture at the moment and is capable of learning long-term dependencies.

The state of an LSTM cell is similar to the hidden state of an RNN cell, but it is a much higher-dimensional vector.
Each cell also has three gates: an input gate, a forget gate and an output gate. These gates control the flow of information into and out of the cell state, and they allow LSTM cells to effectively learn long-term dependencies.

RNNs are used when processing data that requires a consistent understanding, such as natural language. In the case of the code, things are more complicated because the interconnected tokens can be located at a large distance. A single token can affect the change of the entire code block, its operability, and its semantics. This mechanism is flawed due to the \textit{Vanishing Gradient Problem} \cite{vanishing-gradient}, which leaves the model's state at the end of a long sentence without precise, extractable information about preceding tokens.

\textit{LSTM} blocks are designed to improve information transfer from previous iterations of an RNN. However, the main problem with LSTM blocks is that the effect of previous states on the current state decreases exponentially with the distance between the words. The \textit{Attention Mechanism} improves this factor to linear.

It is difficult to train RNNs efficiently because the dependency of token computations on results of previous token computations makes it hard to parallelize computation on modern deep learning hardware \cite{vaswani2017attention}.

For these two reasons described above, RNNs are not the best option when working with code.

\subsection{Transformer}\label{sec:transformer}
The RNN's successor and outperforming replacement became an architecture called \textit{Transformer} \cite{vaswani2017attention}.
Transformers are designed to handle sequential input data (they do not use recurrent blocks and use the Attention Mechanism), but they don't necessarily process it in order as RNNs do.
Therefore, it can more easily parallelize the training process than RNNs because it doesn't have to process the data in sequential order.

The original Transformer proposed in work "Attention Is All You Need" works on the encoder-decoder basis.
Each encoder and decoder have self-attention. In addition, the decoder has another attention mechanism over the encoder layers.
It also uses a multi-head attention mechanism to allow the model to focus on different parts of the input simultaneously. This is a generalization of the self-attention mechanism, where multiple attention heads are used. Each attention head calculates attention in a different space, and the results are concatenated and projected back to the original space \cite{vaswani2017attention}.

The words of natural language in the sentence are connected to each other and can be represented as a fully connected graph, so the Transformer in this particular case can be called a Graph Neural Network with an attention mechanism \cite{joshi2020transformers}.
Since the source code has the same properties and each token code has some relation and a context impact, this allows this to be applied to the code domain as well.

\subsubsection{Bidirectional Encoder Representations from Transformer (BERT)}
As the name implies, it is a model based on Transformer architecture that has been designed to pre-train the language representations in order to apply them further to various NLP tasks. It can be used for various natural language processing tasks such as text classification, question answering, and text generation.

Unlike other language models, \textit{BERT} \cite{bert} trains context-dependent representations. For example, \textit{word2vec} \cite{word2vec} always generates the same embedding for a word, even if the word is polysemous and its meaning depends on the context. BERT takes into account the context of a sentence and the meaning of the word in it, so the embedding may vary from sentence to sentence.

BERT is an \textit{autoencoder} \cite{autoencoder} trained simultaneously on two tasks: Next Sentence Prediction and Masked Language Modeling.

The backbone of BERT is the encoder stack.
The encoder layers in this model use two-way attention, which allows them to take into account the context on both sides of the token being considered and, thus, more accurately determine the value of that token.
By inputting tokenized pairs of sentences with some tokens hidden, BERT is able to learn a deep bi-directional representation of the language that allows it to better understand the context of a sentence.

The task of predicting the next sentence is a binary classification task. The network is trained to distinguish whether there is a connection between sentences in the text.

An example of a model working with code data based on the RoBERTa architecture (the successor of BERT) is CodeBERT . It has been trained on unimodal and bimodal data and achieves state-of-the-art results on downstream tasks, such as code search and documentation generation \cite{feng2020codebert}. It will be mentioned in the perspective of fine-tuning in the Section \ref{codebert}.

\section{Code Representation Models}

\subsection{Word2vec}

One of the first successful and very important approaches is a group of algorithms called \textit{word2vec} \cite{word2vec}, that are used to produce distributed word embeddings.

\textbf{Continuous Bag-of-Words} and \textbf{continuous Skip-gram} architectures were introduced in \textit{"Efficient Estimation of Word Representations in Vector Space"} \cite{word2vec} by Tomas Mikolov (Figure \ref{fig:cbow_skip}). 
\textit{CBOW} learns faster than \textit{Skip-gram} and better represents more frequent words. At the same time \textit{Skip-gram} have better embeddings for less frequent words and works good with small datasets \cite{word2vec}.
\textit{CBOW} approach is trying to predict the word by a given context words (in original paper the size of the context window for \textit{Skip-gram} is 10 and 5 for \textit{CBOW}), so the input vector consists of one-hot encodings of a context words, while \textit{Skip-gram} predicts the context words by a given word.

To train such a model, we need to create a vocabulary of all unique words in our documents and then create pairs of all words in the context window for all the pre-processed documents. 
Taking the first sentence in this chapter as an example and given window size as 3, it might look like \verb|(one, of)|, \verb|(one, the)|, \verb|(one, first)|, \verb|(of, one)|, \verb|(of, the)|, \verb|(of, first)|, and so on.
When training, the one-hot encoding of the first word in pair is input and the encoding of the second one is ground truth. This training step is made for every pair of words.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{cbow-skip-gram}
    \caption{\textit{CBOW} and \textit{Skip-gram}Architectures \cite{word2vec}.\label{fig:cbow_skip}}
\end{figure}

The activation function is only used in the last layer. It's typically a \textit{softmax} or another probability objective (loss) function, which returns the probability that the word will be in a selected context window.
This task is called \textit{fake} because the neural network will not be fully utilized with her outputs. The resulting embeddings we are interested in are contained in the single hidden layer. The number of neurons in this layer determines the embedding length. The bigger it is, the more information it can contain, but it becomes more difficult to train such a model.

Also, the weights matrix grows linearly with increasing word count in the vocabulary. For example, in the case when embedding length equals 300 and vocabulary size is 50000, the matrix shape will be ($300 \times 50000$). If the dataset is large, it becomes almost impossible to train such a model.

The first improving step is to delete all the frequent words like \verb|the| because it does not affect paired word semantics and can be associated with almost any word. 

The next possible upgrade is to use a \textit{Sub-sampling}. At the step of vocabulary creation, the word will get there with a sample probability:
\[
    P(w) = (\sqrt{\frac{r(w)}{s}} + 1) \cdot \frac{s}{r(w)},
\] 
where $w$ is a word, $r(w)$ is a frequency of $w$ among all the documents in proportion to the number of all words, and $s$ is a sub-sample parameter (default value is $0.001$). 

Sub-sample parameter affects the sub-sampling occurrence. The smaller the value of this parameter, the less chance that the word will get into the vocabulary.

Another important tweak is a \textit{Negative Sampling} \cite{word2vec}. The point is to update only the weights for a specific number of randomly selected samples from some distribution (originally \textit{Unigram}) \cite{n-gram}, instead of all the words. A negative word in this context means a word for which the label is 0. It follows that rather than updating 15M weights, only $z * (k + 1)$ weights will be updated, where $z$ is the embedding length and $k$ is the number of negative samples. In addition, the weights for the input vector in the hidden layer are also updated, so $k$ is incremented.

After this model appeared, it was used everywhere with any tokens, but there was no success with the code because of the reasons stated in Section \ref{section:code_vs_nl}. But such models, for example, help to represent information stored in AST nodes, and then it is possible to work with a more understandable structure and apply Graph models. This model provided the impetus and inspiration for the emergence of models such as \textit{doc2vec} (a generalization of \textit{word2vec}, which uses Distributed Bag of Words instead of CBOW and Distributed Memory \cite{doc2vec}) and \textit{graph2vec} \cite{graph2vec} (which is very similar to \textit{doc2vec}, but instead of sampling context words it samples subgraphs and uses Weisfeiler-Lehman kernel), with each subsequent model being the next level of abstraction and based on the previous ones.

\newpage

\subsection{Code2vec}
\textit{Code2vec} is a path-based attention model for learning distributed embeddings for code snippets.
The model takes a code snippet and a corresponding label, caption, or name as input. A fully connected layer takes the embeddings of each path-context and combines them. The attention weights are learned using the combined context vectors and used to compute a code vector. After that code vector is used to predict the label (Figure \ref{fig:code2vec}) \cite{code2vec}. 

\begin{figure}[H]
    \centering
%    \adjustbox{width=1.2\textwidth, center=\textwidth}{%
    \setlength{\fboxsep}{4pt}%
	\setlength{\fboxrule}{.2pt}%
    \fbox{\includegraphics[width=\textwidth]{code2vec}}
    \caption{code2vec Architecture.\label{fig:code2vec}}
\end{figure}

The main idea is that the distribution of labels can be inferred from syntactic paths.
At the time of publication of this article, the model showed better results than previous works - \texttt{2x} better than \textit{LSTM} and \textit{CNN} with \textit{Attention}.

The model has proven to be good at predicting the name of methods. And due to the attention mechanism, it is possible to understand which paths affected the prediction more.

Furthermore, I believe that the idea of applying syntactic paths can be developed further and can find its application with transformer models.

The model proposed in this paper also works with AST (Section \ref{sec:r-ast-roberta}), but in our case, the path is not extracted from the AST, which would probably simplify the transformer task but would change the preprocessing and learning algorithm. 




