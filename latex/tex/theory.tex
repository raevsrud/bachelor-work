%\chapter{Theoretical Background}
\chapter{Theory}

\newtheorem{definition}{Definition}

\section{Graphs 101}
\begin{definition}
\textbf{Graph} is a pair (\(V\), \(E\)), where \(V\) is a finite set of vertices and \(E\) a finite set of edges (paired vertices). 
Loop in a graph is an edge that joins a vertex to itself.
Graph is called \textbf{directed}, when it has oriented edges, \textbf{undirected} otherwise. In this case we must redefine \(E\) as a set of ordered pairs of vertices as follows: \[ E \subseteq \{(x, y)\, |\,(x, y) \in V^2 \quad \textrm{and} \quad x\ne y \}. \]
\end{definition}
\subsection{Adjacency Matrix}
\begin{definition}
\textbf{Graph Adjacency Matrix} is square $n \times n$ matrix $A$, that represents connections between the nodes in a finite graph.
$A_{ij}$ is equal to one when there is an edge between $i$-th and $j$-th nodes, zero - otherwise. In case of undirected graph the Adjacency Matrix is symmetric.
\end{definition}
\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
\centering
\begin{tikzpicture}[node distance={25mm}, thick, main/.style = {draw, circle}] 
\node[main] (1) {$x_1$}; 
\node[main] (2) [above of=1] {$x_2$};
\node[main] (3) [right of=2] {$x_3$}; 
\node[main] (4) [right of=1] {$x_4$};
\draw[->] (1) -- (2);
\draw[->] (1) -- (4);
\draw[->] (2) -- (1);
\draw[->] (2) -- (3);
\draw[->] (3) -- (1);
\draw[->] (4) -- (2);
\end{tikzpicture} 
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
$A = \begin{pmatrix}
    0&1&0&1\\
    1&0&1&0\\
    1&0&0&0\\
    0&1&0&0\\
\end{pmatrix}$
\end{subfigure}

\caption{Graph and It's Adjacency Matrix $A$.\label{fig:graph_adj_matrix}}
\end{figure}


In the case of large graphs, such an Adjacency Matrix may be vast and sparse. Therefore models are more challenging to learn.

\subsection{Adjacency List}
\begin{definition}
\textbf{Adjacency List} is a collection of unordered lists, which represents a finite graph. Each unordered list within an Adjacency List contains a set of neighbors of particular node.
\end{definition}

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
\centering
\begin{tikzpicture}[node distance={25mm}, thick, main/.style = {draw, circle}] 
\node[main] (1) {$x_1$}; 
\node[main] (2) [above of=1] {$x_2$};
\node[main] (3) [right of=2] {$x_3$}; 
\node[main] (4) [right of=1] {$x_4$};
\draw[->] (1) -- (2);
\draw[->] (1) -- (4);
\draw[->] (2) -- (1);
\draw[->] (2) -- (3);
\draw[->] (3) -- (1);
\draw[->] (4) -- (2);
\end{tikzpicture} 
\end{subfigure} %
\begin{subfigure}{.49\textwidth}
    	\centering
    	\begin{tabular}{|c|c|} 
    	\hline 
    	node & neighbors \\
    	\hline
		$x_1$ & $x_2, x_4$ \\
		\hline
		$x_2$ & $x_1, x_3$ \\
		\hline
		$x_3$ & $x_1$ \\
		\hline
		$x_4$ & $x_2$ \\
		\hline
    	\end{tabular}
\end{subfigure}

\caption{Graph and It's Adjacency List.\label{fig:graph_adj_list}}
\end{figure}

The main advantage of this representation is its small size, and therefore there is no need for a large amount of memory.
Also, this version is more understandable and more transparent than the matrix. 
The only disadvantage of this option is the slow search for edges between the nodes, in fact, $O(|V|)$, while the Adjacency Matrix has a constant time.

\subsection{Incidence Matrix}
\begin{definition}
	\textbf{Incidence Matrix} is $n\times m$ matrix $I$, where $n$ and $m$ are the numbers of nodes and edges respectively. 
	
	For undirected graph $I_{ij} = 1$ if the node $x_i$ and edge $e_j$ are incident, otherwise - $0$. The sum for each column is $2$.
	
	For directed graph $I_{ij} = 1$ if the edge $e_j$ enters vertex $x_i$, $I_{ij} = -1$ if it leaves vertex $x_i$, otherwise - $0$. The sum of any column and row is $0$.
\end{definition}


\begin{figure}[H]
\centering
\begin{subfigure}{.4\textwidth}
\centering
\begin{tikzpicture}[auto, node distance={25mm}, thick, main/.style = {draw, circle}] 
\node[main] (1) {$x_1$}; 
\node[main] (2) [above of=1] {$x_2$};
\node[main] (3) [right of=2] {$x_3$}; 
\node[main] (4) [right of=1] {$x_4$};
\draw[->] (1) edge node[midway, above right, sloped]{$e_1$} (2);
\draw[->] (1) edge node[midway, above right, sloped]{$e_2$} (4);
\draw[->] (2) edge node[midway, above right, sloped]{$e_3$} (1);
\draw[->] (2) edge node[midway, above right, sloped]{$e_4$} (3);
\draw[->] (3) edge node[midway, above right, sloped]{$e_5$} (1);
\draw[->] (4) edge node[midway, above right, sloped]{$e_6$} (2);
\end{tikzpicture} 
\end{subfigure}%
\begin{subfigure}{.6\textwidth}
    	\centering
    	\begin{tabular}{|l|c|c|c|c|c|c|} 
    	\hline
    	& $e_1$ & $e_2$ & $e_3$ & $e_4$ & $e_5$ & $e_6$  \\
		\hline
		$x_1$ & 1 & 1 & -1 & 0 & -1 & 0 \\
		\hline
		$x_2$ & -1 & 0 & 1 & 1 & 0 & -1 \\
		\hline
		$x_3$ & 0 & 0 & 0 & -1 & 1 & 0 \\
		\hline
		$x_4$ & 0 & -1 & 0 & 0 & 0 & 1 \\
		\hline
    	\end{tabular}
\end{subfigure}
\caption{Graph and It's Incidence Matrix $I$.\label{fig:graph_inc_matrix}}
\end{figure}

\subsection{Complexity Comparison}
Each of the described graph representations has its trade-offs, so the choice depends on the concrete situation.

\begin{table}[H]
	\begin{adjustbox}{center}
	\begin{tabular}{|c|c|c|c|c|}
		\hline
		Operation & Adjacency Matrix & Adjacency List & Incidence Matrix & Incidence List\\
		\hline
		Add node & $O(|V|^2)$ & $O(1)$ & $O(|V|\cdot |E|)$ & $O(1)$ \\
		\hline
		Remove node & $O(|V|^2)$ & $O(|V| + |E|)$ & $O(|V|\cdot |E|)$ & $O(|E|)$ \\
		\hline
		Add edge & $O(1)$ & $O(1)$ & $O(|V|\cdot |E|)$ & $O(1)$\\
		\hline
		Remove edge & $O(1)$ & $O(|E|)$ & $O(|V|\cdot |E|)$ & $O(|E|)$ \\
		\hline
		Query edge & $O(1)$ & $O(|V|)$ & $O(|E|)$ & $O(|E|)$ \\
		\hline
	\end{tabular}
	\end{adjustbox}
	\caption{Graph Operation Time Complexity.\label{fig:graph_time_complexity}}
\end{table}


As a rule, matrix representations take up much more memory but have an advantage in speed of operations, the lists vice versa.


\begin{table}[H]
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		Adjacency Matrix & Adjacency List & Incidence Matrix & Incidence List \\
		\hline
		$O(|V|^2)$ & $O(|V| + |E|)$ & $O(|V|\cdot |E|)$ & $O(|V| + |E|)$ \\
		\hline
	\end{tabular}
	\caption{Space Complexity (Average Case).\label{fig:graph_space_complexity}}
\end{table}

%\section{Code Representation}
\section{Source Code Representation}

\subsection{Tokens}
During the \textit{lexical analysis}, the lexer converts a sequence of characters into a sequence of tokens.
Token, or in terms of PL, also called lexeme (often comparable to words in natural language), usually consists of its type or name and value. Common token types are:
\begin{description}
    \item[Identifier] Usually the name of variable/function/class
    \item[Keyword] Programming Language reserved word (e.g. \verb|for|, \verb|function|, \verb|while|)
    \item[Literal] Numeric, logical, string, other literals (e.g. \verb|TRUE|, \verb|"Hello, World!"|, \verb|1234.5|)
    \item[Operator] Operation Symbol (e.g. \verb|+|, \verb|-|)
    \item[Separator] Punctuation Character (e.g. \verb|;|, \verb|:|, \verb|(|, \verb|{|)
    \item[Comment] Lines ignored by the compiler usually starting with symbols \verb|//|, \verb|#| or multi-line comments \verb|/** */|
\end{description}

\subsection{Abstract Syntax Tree}
Abstract syntax tree (AST) is a data structure representing the syntactic structure and the source code in terms of its formal grammar.
The compiler usually represents the code as ASTs at a particular stage of its syntax analysis (the step after lexical analysis), so this type of representation is essential.

Conversion of source code into AST is a reversible operation, so the order of expressions and operand positions must be explicit.
Each node in the tree has its identifier, type, and value.
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{ast-example}
    \caption{Abstract Syntax Tree Example \cite{property_graphs}.\label{fig:ast}}
\end{figure}

\subsection{Dependency Graph}
A \textit{Dependency Graph} is a directed graph that represents dependencies between objects. It has a widely used representation form in computer science. In programming languages, simple examples are data dependencies and control dependencies graphs. They can be used in compiler optimizations to find and eliminate the dead code (code that can not be executed and unused variables).

\subsection{Program Dependence Graph}

\textit{Program Dependence Graph} (PDG, Figure \ref{fig:pdg}) is an intermediate program representation that makes data and controls dependencies explicit \cite{dpg}. It is used by most compilers in order to make transformations during optimization steps. 

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.6]{img/ast-example}
    \caption{Program Dependence Graph Example \cite{property_graphs}.}
    \label{fig:pdg}
\end{figure}

\subsection{Control Flow Graph}
\textit{Control Flow Graph} (CFG, Figure \ref{fig:cfg}) represents the order of code execution and conditions for each node (single instruction) \cite{control_flow_analysis}.
This type of graph is essential to many compiler optimizations and static analysis tools.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.4]{cfg-example}
    \caption{Control Flow Graph Example \cite{cfg}.}
    \label{fig:cfg}
\end{figure}

\subsection{Code Property Graph}
\textit{Code Property Graph} (CPG, Figure \ref{fig:cpg}) is a combination of AST, CFG, and DPG. Such intermediate representation is independent of the programming language. This robust and comprehensive data structure is widely used in source code analysis, searching for bugs and vulnerabilities.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{cpg-example}
    \caption{Code Property Graph Example.\label{fig:cpg}}
\end{figure}

\section{Natural Language Processing}

\subsection{Word Embeddings}

\begin{definition}
Word embedding $e \in V$ is a learned distributed vector representation of a given word $w$, that maps it into Vector Space $V$.
\end{definition}
Embedding usually has a few hundred dimensions, unlike sparse methods, which have several dimensions proportional to the number of words in a corpus.

\textbf{The Distributional Hypothesis} proposed in paper \textit{Distributional Structure} by Zellig S. Harris \cite{distributionalstructure} says \textit{"Words that occur in the same contexts tend to have similar meanings"}, so such words would have similar embeddings. 
 
Methods, such as \textit{word2vec} \cite{word2vec}, Autoencoders, and embedding layers in Dense Neural Networks are typically used to learn real-valued embeddings for a predefined and fixed-sized vocabulary from a text corpus. However, there are also other approaches, which will be discussed later.

\subsection{N-gram}
\begin{definition}
N-gram is a contiguous sequence of n tokens (words) from a given string.
\end{definition}
For example, \textit{unigram} is just one word, \textit{bigram} - two, and so on.
\textit{N-gram models} \cite{n-gram} use \textit{Markov model} \cite{markov} as an approximation: we make an assumption, that each word depends only on the last n - 1 words. 

\subsection{Bag-of-Words}
The first reference to this term in the NLP context was found in the already mentioned work of Zellig Harris \cite{distributionalstructure}. BoW model is a bag (or multiset) representation of words that represents the word frequency within a document. Word order information is not used, making this method very simple, unlike pre-processing the data before using this method.

\textbf{The algorithm} looks as follows:
\begin{enumerate}[topsep=0ex]
    \item Create a vocabulary with unique words from all the documents. Each word will be associated with a specific position in future vectors.
    \item The next step is to score each document, and there are several ways to do this.
    \begin{enumerate}
        \item The first is to determine whether a word from vocabulary is in the document or not.
        \item Another way of doing this is to count the occurrences of each word.
        \item Calculate the word's frequency of occurrence in the document relative to all other words in the same document.
    \end{enumerate}
\end{enumerate}

The most interesting part is pre-processing. Some words occur very frequently and do not affect semantics, so it is necessary to get rid of them. These words are called stop words (e.g. \verb|"a"|, \verb|"the"|, \verb|"an"|, \verb|"of"|). The lists of stop words for all major languages are freely available. Also, the same word can be used in different forms (e.g., words like \verb|"drinking"| and \verb|"drink"|), so you need to reduce them to their stem.

\textbf{Pre-processing pipeline} might look as follows:

\begin{enumerate}[parsep=0ex, itemsep=0ex, topsep=0ex, align=left]
    \item Convert all words to the same case.
    \item Remove punctuation.
    \item Remove stop words.
    \item Apply stemming algorithm.
    \item Apply lemmatisation algorithm.
\end{enumerate}


%\section{Graph Neural Networks and Graph Embedding}
\section{Graph Neural Networks}

Graph Neural Network is designed to operate on graph data, preserving graph symmetries.

Graph level and node level embeddings are more complex than word embeddings due to structural information. The goal of a graph-level task is to predict the property of an entire graph.
The node-level tasks focus on predicting the identity or role of each node in the graph.

When it comes to a graph, the graph's topology, the neighborhood of each node, node connections, and node features play a major role.

There are \textit{spatial} algorithms that use all the information, including internal information from the nodes (e.g., embedding for a token that the node represents, or code token in our case), for example, \textit{Graph2Vec} \cite{graph2vec}.

The other type is \textit{spectral}, and it uses only general information about the graph and its structure (e.g., node degrees, clustering coefficient, adjacency matrix, etc.), for example, \textit{FeatherGraph} \cite{feather}.







