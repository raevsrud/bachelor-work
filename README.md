# Data

- [R Dataset](https://huggingface.co/datasets/lanesket/r-lang-dataset)
- [R AST Dataset](https://huggingface.co/datasets/lanesket/r-asts)
- [R AST Tokenized Dataset](https://huggingface.co/datasets/lanesket/r-asts-tokenized)
- [R AST Augmented Datset](https://huggingface.co/datasets/lanesket/r-asts-splitted)
- [R AST Augmented Tokenized Dataset](https://huggingface.co/datasets/lanesket/r-asts-splitted-tokenized)
- [RASTaBERTa](https://huggingface.co/lanesket/RASTaBERTa)

[RASTaBERTa Demo on Google Colab](https://colab.research.google.com/drive/1JZmB_bXKitItprV7WXUuCcYbSRnL1Alb?usp=sharing)