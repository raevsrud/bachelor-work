import os.path as osp
import torch
from torch_geometric.data import InMemoryDataset, Dataset, Data
from preprocess import *
from gensim.models.doc2vec import Doc2Vec
from nltk.tokenize import word_tokenize
from tqdm import tqdm
import pydot
import numpy as np
import networkx as nx
import concurrent.futures
import logging

logging.basicConfig(filename="CodeDataset.log",
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)


class CodeDatasetFromNetx(Dataset):
    def __init__(self, root, transform=None, pre_transform=None):
        super().__init__(root, transform, pre_transform)

    @property
    def raw_file_names(self):
        paths = [f"{self.root}/chunk_{i}" for i in range(6)]
        res = []
        for rp in paths:
            pkls = [x for x in os.listdir(rp) if "info" not in x]
            pkls = sorted(pkls, key=lambda k: int(k.rstrip(".pkl")))
            res += [f"{rp}/{p}" for p in pkls]
        return res

    @property
    def processed_file_names(self):
        paths = [f"{self.root}/chunk_{i}" for i in range(6)]
        res = []
        for i, rp in enumerate(paths):
            pkls = [x for x in os.listdir(rp) if "info" not in x]
            pkls = sorted(pkls, key=lambda k: int(k.rstrip(".pkl")))
            res += [f"chunk_{i}/" + p.replace(".pkl", ".pt") for p in pkls]
        return res

    def process_netx_path(self, idx):
        graph_path = self.raw_file_names[idx]
        if os.path.exists(self.processed_paths[idx]):
            logging.info(f"{self.processed_paths[idx]} already exists.")
            return False

        g = np.load(graph_path, allow_pickle=True)
        features = []
        for node_idx in g.nodes:
            feature = g.nodes[node_idx]["feature"]
            feature = np.concatenate([x.ravel() for x in feature])
            features.append(feature)

        x = torch.tensor(np.array(features), dtype=torch.float)
        edges = [list(e) for e in g.edges]
        edge_index = torch.tensor(edges, dtype=torch.long).t().contiguous()
        data = Data(x=x, edge_index=edge_index)

        if self.pre_transform is not None:
            data = self.pre_transform(data)

        torch.save(data, self.processed_paths[idx])
        logging.info(f"{self.processed_paths[idx]} already exists.")

        return True

    # def process(self):
    #     idxs = range(len(self.raw_file_names))
    #     max_workers = 9
    #     with concurrent.futures.ProcessPoolExecutor(max_workers=max_workers) as executor:
    #         pool = tqdm(executor.map(self.process_netx_path,
    #                     idxs, chunksize=32), total=len(idxs))

    def process(self):
        for idx, graph_path in tqdm(enumerate(self.raw_file_names), total=len(self.raw_file_names)):
            if os.path.exists(self.processed_paths[idx]):
                logging.info(f"{self.processed_paths[idx]} already exists.")
                continue

            g = np.load(graph_path, allow_pickle=True)
            features = []
            for node_idx in g.nodes:
                feature = g.nodes[node_idx]["feature"]
                feature = np.concatenate([x.ravel() for x in feature])
                features.append(feature)

            x = torch.tensor(np.array(features), dtype=torch.float)
            edges = [list(e) for e in g.edges]
            edge_index = torch.tensor(edges, dtype=torch.long).t().contiguous()
            data = Data(x=x, edge_index=edge_index)

            if self.pre_filter is not None and not self.pre_filter(data):
                continue

            if self.pre_transform is not None:
                data = self.pre_transform(data)

            torch.save(data, self.processed_paths[idx])
            logging.info(f"{self.processed_paths[idx]} saved.")

    def len(self):
        return len(self.processed_file_names)

    def get(self, idx):
        return torch.load(self.processed_paths[idx])


if __name__ == "__main__":
    dataset = CodeDatasetFromNetx('ast_netx')
