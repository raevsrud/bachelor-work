library(asttoolsr)

x <- 0
for (path in list.files("r_data", pattern = ".*\\.[rR]$", recursive=TRUE, full.names=TRUE)) {
  tryCatch(
    expr = {
      ast <- parse(file = path)
      dot_path <- gsub("\\.[rR]", ".dot", path)
      ast_to_dot_file(ast, dot_path)
      x <- x + 1
      print(paste("Done: ", dot_path))
    },
    error = function(e) { 
      print(paste("Error: ", path))
      print(e)
    })
}

print(paste("Done: ", x))