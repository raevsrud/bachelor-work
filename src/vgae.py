from torch_geometric.nn import GCNConv
from torch_geometric.nn import GAE, VGAE
import torch.nn.functional as F
from preprocess import *
import numpy as np
import torch
import torch.nn as nn

import torch_geometric.data as data

class GCNEncoder(torch.nn.Module):
    def __init__(self, in_channels, out_channels):
        super(GCNEncoder, self).__init__()
        self.conv1 = GCNConv(in_channels, 2 * out_channels, cached=True)
        self.conv2 = GCNConv(2 * out_channels, out_channels, cached=True)

    def forward(self, x, edge_index):
        x = self.conv1(x, edge_index).relu()
        x = F.dropout(x, training=self.training)
        return self.conv2(x, edge_index)

class VariationalGCNEncoder(torch.nn.Module):
    def __init__(self, in_channels, out_channels):
        super(VariationalGCNEncoder, self).__init__()
        self.conv1 = GCNConv(in_channels, 2 * out_channels, cached=True) # cached only for transductive learning
        self.conv_mu = GCNConv(2 * out_channels, out_channels, cached=True)
        self.conv_logstd = GCNConv(2 * out_channels, out_channels, cached=True)

    def forward(self, x, edge_index):
        x = self.conv1(x, edge_index).relu()
        return self.conv_mu(x, edge_index), self.conv_logstd(x, edge_index)

from torch_geometric.loader import DataLoader
from CodeDataset import CodeDataset

dataset = CodeDataset("data")

loader = DataLoader(dataset, batch_size=100)
device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
print(device)

num_features = loader.dataset[0].num_features
out_channels = 128
print(num_features)

model = VGAE(VariationalGCNEncoder(num_features, out_channels))

num_epochs = 100
batch = data.Batch().from_data_list([dataset[i] for i in tqdm(range())])

optimizer = torch.optim.Adam(model.parameters(), lr=0.0005)
losses = []
for epoch in range(num_epochs):
    model.train()
    optimizer.zero_grad()
    output = model.encode(batch.x, batch.edge_index)
    loss = model.recon_loss(output, batch.edge_index)
    loss = loss + (1 / batch.num_nodes) * model.kl_loss()  
    loss.backward()
    optimizer.step()
    losses.append(loss.item())

    if epoch % 10 == 0:
        print(f"{epoch} -> {sum(losses)/len(losses)}")