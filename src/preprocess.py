import os
import pydot
import torch
from typing import Dict, List, Optional
from tqdm import tqdm


def get_dot_paths(graphs_dir: str):
    dot_files = []
    for file in os.listdir(graphs_dir):
        if file.endswith(".dot") and (os.path.getsize(os.path.join(graphs_dir, file)) / (1024 ** 2) < 1):
            # dot_files.append(os.path.join(graphs_dir, file))
            dot_files.append(file)
    return dot_files

def get_pt_paths(graphs_dir: str):
    pt_files = []
    for file in os.listdir(graphs_dir):
        if file.endswith(".dot"):
            pt_file = file.replace(".dot", ".pt")
            pt_files.append(pt_file)
    return pt_files


def get_graphs(dot_files: List[str], n: Optional[int] = None) -> List[pydot.Dot]:
    n = n if n is not None else len(dot_files)
    print(f"Parsing {n} graphs.")
    return [pydot.graph_from_dot_file(f)[0] for f in tqdm(dot_files[:n])]


def new_graph_mapping(graph: pydot.Dot, start: int) -> Dict[str, int]:
    nodes = graph.get_node_list()
    res, i = {}, start  
    for node in nodes:
        name = node.get_name()
        if name not in res:
            res[name] = i
            i += 1
    return res, start + len(nodes)
    # return {node.get_name(): i for (i, node) in enumerate(nodes, start=start)}, start + len(nodes)


def remap_graph(graph: pydot.Dot, mapping: Dict[str, int]) -> None:
    for node in graph.get_node_list():
        node.set_name(mapping[node.get_name()])

    for edge in graph.get_edges():
        edge.obj_dict["points"] = mapping[edge.obj_dict["points"][0]], mapping[edge.obj_dict["points"][1]]


def get_edge_index(graph: pydot.Dot) -> torch.Tensor:
    edges = []
    for edge in graph.get_edge_list():
        src, dst = edge.get_source(), edge.get_destination()
        edges.append([src, dst])
        # edges.append([dst, src])
    edges_index = torch.tensor(edges, dtype=torch.long).t().contiguous()
    return edges_index