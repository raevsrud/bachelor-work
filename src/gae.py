from torch_geometric.nn import GCNConv
import torch.nn as nn
import torch.nn.functional as F
import torch
 

class GCNEncoder(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(GCNEncoder, self).__init__()
        self.conv1 = GCNConv(in_channels, 2 * out_channels, cached=True)
        self.conv2 = GCNConv(2 * out_channels, out_channels, cached=True)

    def forward(self, x, edge_index):
        x = self.conv1(x, edge_index).relu()
        x = F.dropout(x, training=self.training)
        return self.conv2(x, edge_index)

class VariationalGCNEncoder(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(VariationalGCNEncoder, self).__init__()
        self.conv1 = GCNConv(in_channels, 2 * out_channels, cached=True) # cached only for transductive learning
        self.conv_mu = GCNConv(2 * out_channels, out_channels, cached=True)
        self.conv_logstd = GCNConv(2 * out_channels, out_channels, cached=True)

    def forward(self, x, edge_index):
        x = self.conv1(x, edge_index).relu()
        return self.conv_mu(x, edge_index), self.conv_logstd(x, edge_index)

def train(data, model, optim, vgae=False, device=None):
    model.train()
    optim.zero_grad()
    if device:
        data = data.to(device)
    output = model.encode(data.x, data.edge_index)
    if vgae:
        loss = model.recon_loss(output, data.edge_index)
        loss = loss + (1 / data.num_nodes) * model.kl_loss()  
    else:
        loss = model.recon_loss(output, data.edge_index)
    loss.backward()
    optim.step()

    return loss.item()

def test(model, pos_edge_index, neg_edge_index):
    model.eval()
    with torch.no_grad():
        z = model.encode(x, train_pos_edge_index)
    return model.test(z, pos_edge_index, neg_edge_index)