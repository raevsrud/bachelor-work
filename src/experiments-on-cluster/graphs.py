import lzma
import pickle
import numpy as np
import networkx as nx
from networkx.drawing.nx_agraph import read_dot
import pydot
import os
import re
from gensim.models.doc2vec import Doc2Vec
from sklearn.preprocessing import MultiLabelBinarizer


EXT = '.xz'
def save_graph(graph, filename):
    if not filename.endswith(EXT):
        filename += EXT
    with lzma.open(filename, "wb") as f:
        pickle.dump(graph, f)
                    
def load_graph(path):
    if not path.endswith(EXT):
        path += EXT
    with lzma.open(path, "rb") as f:
        return pickle.load(f)
    
def parse_node_content(content):
    m = re.match(r'\[([^\]]*)\]\(([^\)]*)\)\{(.*)\}', content)  # https://regex101.com/r/jbxUVX/1
    if m is None:
        return None
    semantic_info = ''.join([i for i in m.group(1) if not i.isdigit()]) # get rid of numbers
    semantic_info = [x.strip() for x in semantic_info.split(";")]
    if semantic_info == [""]:
        semantic_info = None
    val_type = m.group(2)
    if val_type == "":
        val_type = None
    value = m.group(3)

    return {
        'raw': content,
        'semantic_info': semantic_info,
        'value_type': val_type,
        'value': value
    }

def features_flat(node_features):
    return np.concatenate([x.ravel() for x in node_features])

def extract_graph_info(graph, dot_path, mlb, doc2vec):
    for node_name in graph.nodes:
        node = graph.nodes[node_name]
        graph.graph['dot_path'] = dot_path
        if 'label' not in node:
            continue
        
        content = node['label']
        p = parse_node_content(content)
        node['raw'] = p['raw']
        node['semantic_info'] = p['semantic_info']
        node['value_type'] = p['value_type']
        node['value'] = p['value']
        del node['label']
        
        if node['semantic_info']:
            semantic_info_vector = mlb.transform([node['semantic_info']])[0]
        else:
            semantic_info_vector = mlb.transform([[]])[0]

        if node['value_type']:
            value_type_vector = mlb.transform([[node['value_type']]])[0]
        else:
            value_type_vector = mlb.transform([[]])[0]

        value_vector = doc2vec.infer_vector([node['value']])

        node['feature'] = features_flat([semantic_info_vector, value_type_vector, value_vector])

def process_dot(dot_path, mlb, doc2vec, i=None):
    try:
        graph = read_dot(dot_path)
        graph = nx.DiGraph(graph)
        if i:
            graph = nx.convert_node_labels_to_integers(graph, first_label=i)
        else:
            graph = nx.convert_node_labels_to_integers(graph)

        extract_graph_info(graph, dot_path, mlb, doc2vec)
    
        return graph
    except:
        return None

def n_match(n1, n2):
    if n1['raw'] != n2['raw']:
        return False
    return True

def graphs_equal(g1, g2):
    un_g1 = g1 if not g1.is_directed() else g1.to_undirected()
    un_g2 = g2 if not g2.is_directed() else g2.to_undirected()
    if 0 not in un_g1.nodes:
        un_g1 = nx.convert_node_labels_to_integers(un_g1)
    if 0 not in un_g1.nodes:
        un_g1 = nx.convert_node_labels_to_integers(un_g1)
    return nx.is_isomorphic(un_g1, un_g2, node_match=n_match)

def get_subgraph_by_node(undirected_graph, node_id: int):
    component_nodes = nx.node_connected_component(undirected_graph, node_id)
    return undirected_graph.subgraph(component_nodes)

def get_root_node(graph, calc=False):
    if calc:
        return [n for n, d in graph.in_degree() if d == 0][0]
    return list(graph.nodes)[0]  # First node in our case as AST already traversed


def get_traversal(graph, traversal_func=None):
    if traversal_func:
        root_node = get_root_node(graph)
        return list(traversal_func(graph))
    return list(graph.nodes)

def get_ast_seq(graph, end_leaf_token, ast_sep_token=None, concat=False):
    seq = []
    traversal = get_traversal(graph, nx.dfs_preorder_nodes)
    leaf_end_pos = leaf_end_positions(graph)

    for i, node_name in enumerate(traversal):
        node = graph.nodes[node_name]
        label = node['label']
        seq.append(label)
        
        if node_name in leaf_end_pos:
            seq.append(end_leaf_token)
        
        elif ast_sep_token and i != len(traversal) - 1:
            seq.append(ast_sep_token)
            
    if concat:
        seq = ''.join(seq)
    return seq

def get_leaves(graph):
    return [v for v, d in graph.out_degree() if d == 0]

def leaf_end_positions(graph: nx.DiGraph):
    leaves = get_leaves(graph)
    un_g = graph.to_undirected() # temporary
    positions = set()
    for lfn1, lfn2 in zip(leaves, leaves[1:]):
        r1 = list(un_g.neighbors(lfn1))[0]
        r2 = list(un_g.neighbors(lfn2))[0]
        
        # that means that r1 is the last leaf for their root
        if r1 != r2: 
            positions.add(lfn1)

    return positions

def paths(graph):
    leaves = (v for v, d in graph.out_degree() if d == 0)
    all_paths = []

    for leaf in leaves:
        paths = nx.all_simple_paths(graph, root, leaf)
        all_paths.extend(paths)
    return all_paths

def dot_file2netx(path):
    return nx.convert_node_labels_to_integers(nx.DiGraph(read_dot(path)))