import networkx as nx
from concurrent.futures import process
import concurrent.futures
import os
import torch
from torch_geometric.data import Dataset, Data
from tqdm import tqdm
import logging
import numpy as np

logging.basicConfig(filename="CodeDataset.log",
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)

def raw_file_names():
    paths = [f"ast_netx/chunk_{i}" for i in range(10)][7:]
    res = []
    for rp in paths:
        pkls = [x for x in os.listdir(rp) if "info" not in x]
        pkls = sorted(pkls, key=lambda k: int(k.rstrip(".pkl")))
        res += [f"{rp}/{p}" for p in pkls]
    return res

def processed_paths():
    paths = [f"ast_netx/chunk_{i}" for i in range(10)][7:]
    res = []
    for i, rp in enumerate(paths):
        pkls = [x for x in os.listdir(rp) if "info" not in x]
        pkls = sorted(pkls, key=lambda k: int(k.rstrip(".pkl")))
        res += [f"ast_netx/processed/chunk_{i+7}/" + p.replace(".pkl", ".pt") for p in pkls]
    return res

raw_file_names = raw_file_names()
processed_paths = processed_paths()


def process_netx_path(self, idx):
    graph_path = raw_file_names[idx]
    # if os.path.exists(processed_paths[idx]):
    #     logging.info(f"{processed_paths[idx]} already exists.")
    #     return False

    g = np.load(graph_path, allow_pickle=True)
    features = []
    for node_idx in g.nodes:
        feature = g.nodes[node_idx]["feature"]
        feature = np.concatenate([x.ravel() for x in feature])
        features.append(feature)

    x = torch.tensor(np.array(features), dtype=torch.float)
    edges = [list(e) for e in g.edges]
    edge_index = torch.tensor(edges, dtype=torch.long).t().contiguous()
    data = Data(x=x, edge_index=edge_index)

    torch.save(data, processed_paths[idx])
    logging.info(f"{processed_paths[idx]} saved.")

    return True

# idxs = range(len(raw_file_names))
# max_workers = 9
# with concurrent.futures.ProcessPoolExecutor(max_workers=max_workers) as executor:
#     pool = tqdm(executor.map(process_netx_path,
#                 idxs, chunksize=32), total=len(idxs))

for idx, graph_path in tqdm(enumerate(raw_file_names), total=len(raw_file_names)):
    # if os.path.exists(processed_paths[idx]):
    #     logging.info(f"{processed_paths[idx]} already exists.")
    #     continue

    g = np.load(graph_path, allow_pickle=True)
    features = []
    for node_idx in g.nodes:
        feature = g.nodes[node_idx]["feature"]
        feature = np.concatenate([x.ravel() for x in feature])
        features.append(feature)

    x = torch.tensor(np.array(features), dtype=torch.float)
    edges = [list(e) for e in g.edges]
    edge_index = torch.tensor(edges, dtype=torch.long).t().contiguous()
    data = Data(x=x, edge_index=edge_index)

    torch.save(data, processed_paths[idx])
    logging.info(f"{processed_paths[idx]} saved.")